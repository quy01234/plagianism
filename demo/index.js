// =======================
// get the packages we need ============
// =======================
var express = require('express');
var app = express();

var server = require('http').createServer();

var config = require('./config'); // get our config file

var api = require('./controllers/index.js');

// =======================
// routes ================
// =======================
// basic route
//app.get('/',home);


// =======================
// configuration =========
// =======================
var configs = require('./helpers/config.js');
configs.setup(app);

// API ROUTES -------------------
// get an instance of the router for api routes
var apiRoutes = express.Router();

apiRoutes.use('/', api);

// apply the routes to our application with the prefix /api
app.use('/api', apiRoutes);

// =======================
// start the server ======
// =======================
app.listen(config.EXPRESSPORT, function (err) {
    console.log('server express listen socketio on ', config.EXPRESSPORT);
});
