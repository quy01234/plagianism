module.exports = {
    'documentServer': 'http://localhost:8081/',
    'sentenceServer': 'http://localhost:8080/',
    'addPath': 'add',
    'getPath': 'get',
    'searchPath': 'search'
};
