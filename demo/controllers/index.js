// basic route
// =======================
// load all controller ================
// =======================

var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
  res.json({ message: 'Welcome to the coolest API on earth!' });
});

router.use('/documents',require('./document.js'));



module.exports = router;