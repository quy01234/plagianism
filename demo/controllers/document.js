// =======================
// Users controller ================
// =======================

var express = require('express');
var router = express.Router();
var request = require('request');
var Tokenizer = require('sentence-tokenizer');
var os = require("os");
const uuid = require('uuid');
const contances = require('../const.js');
var StringBuilder = require('string-builder');
const qs = require('querystring');
var sentenceHelper = require('../helpers/sentence.js');
var Seq = require('seq');


router.post('/', function(req, res) {

    var tokenizer = new Tokenizer();
    var sb = new StringBuilder();

    
    var content = req.body.body.replace(os.EOL, '. ');

    tokenizer.setEntry(content);

    var sentences = tokenizer.getSentences();

    var uid = uuid.v1();

    var length = sentences.length;

    if (length == 0)
    {
        res.json({ success: false, message: 'body too short' }).end();
        return;
    }
    
    var documentObject = {
        title: req.body.title,
        url: req.body.url,
        author: req.body.author,
        description: req.body.description,
        id: uid,
        sentencesNum: length
    };

    request(
      {
          method: 'POST'
      , uri: contances.documentServer + contances.addPath
      , body: JSON.stringify(documentObject)
      }
    , function (error, response, body) {
        // body is the decompressed response body
        //console.log('server encoded the data as: ' + (response.headers['content-encoding'] || 'identity'))
        //console.log('the decoded data is: ' + body)

        if (error) {
            res.json({ success: false, error: error }).end();
            return;
        }

        //ok
        if (body.endsWith('{"event":"finished"}\n')) {

            sb.append('{"sentence": "' + sentences[0] + '","order":"' + 0 + '","docId":"' + uid + '"}')
            for (var i = 1; i < length; i++) {
                sb.appendLine('{"sentence": "' + sentences[i] + '","order":"' + i + '","docId":"' + uid + '"}');
            }

            request(
      {
          method: 'POST'
      , uri: contances.sentenceServer + contances.addPath
      , body: sb.toString()
      }
    , function (error, response, body) {
        // body is the decompressed response body
        //console.log('server encoded the data as: ' + (response.headers['content-encoding'] || 'identity'))
        //console.log('the decoded data is: ' + body)

        if (error) {
            res.json({ success: false, error: error }).end();
            return;
        }

        //ok
        if (body.endsWith('{"event":"finished"}\n')) {

            sb.clear();
            sb = undefined;

            res.json({ success: true, documentId: uid });
        }




    }
    ).on('data', function (data) {
        // decompressed data as it is received
        //console.log('decoded chunk: ' + data)
    })
    .on('response', function (response) {
        // unmodified http.IncomingMessage object
        response.on('data', function (data) {
            // compressed data as it is received
            //console.log('received ' + data.length + ' bytes of compressed data')
        })
    });
        }




    }
    ).on('data', function (data) {
        // decompressed data as it is received
        //console.log('decoded chunk: ' + data)
    })
    .on('response', function (response) {
        // unmodified http.IncomingMessage object
        response.on('data', function (data) {
            // compressed data as it is received
            //console.log('received ' + data.length + ' bytes of compressed data')
        })
    });

});

router.get('/document/:id', function (req, res) {
    if (!req.params.id)
    {
        res.json("id must not null").end();
        return;
    }
    var ids = '["'+req.params.id+'"]';

    request(
{
    method: 'GET'
, uri: contances.documentServer + contances.getPath + '?' + qs.stringify({ ids: ids })
}
, function (error, response, body) {
    // //body is the decompressed response body
    //console.log('server encoded the data as: ' + (response.headers['content-encoding'] || 'identity'))
    //console.log('the decoded data is: ' + body)

    if (error) {
        res.json({ success: false, error: error }).end();
        return;
    }
  
    if (!body || body.trim() == "")
    {
        res.json({success:false, message: 'cant find this document'});
    }
    else
    {
        res.json(JSON.parse(body));
    }

}
).on('data', function (data) {
    //// decompressed data as it is received
    //console.log('decoded chunk: ' + data)
})
.on('response', function (response) {
    // unmodified http.IncomingMessage object
    response.on('data', function (data) {
        //// compressed data as it is received
        //console.log('received ' + data.length + ' bytes of compressed data')
    })
});
});

router.post('/check', function (req, res) {

    var checkObject = new Object();
    var contentLength = 0;
    var tokenizer = new Tokenizer();
    var sentences = sentenceHelper.removeStopwords(req.body.content);
    tokenizer.setEntry(sentences);
    var sentenceArray = tokenizer.getSentences();


    Seq(sentenceArray).seqEach(function (x, index) {
        
        var kgramWords = sentenceHelper.wordskgram(x);

        var kwordsLength = kgramWords.length;
        
        //@Todo: not fix kgram value
        if (kwordsLength < 2)//2 is ngram
        {
            this();
        }
        else
        {
            contentLength += kwordsLength;

            var sb = new StringBuilder();
            sb.append('{"query":[');

            wordQuerry = '{"AND": { "sentence": [ "' + kgramWords[0] +'"]}}';
            sb.append(wordQuerry);
            
            for(var i=1; i< kwordsLength;i++)
            {
                wordQuerry = ',{"AND": { "sentence": [ "' + kgramWords[i] +'"]}}';
                sb.append(wordQuerry);
            }

            sb.append(']}');

            request(
            {
                method: 'GET'
            , uri: contances.sentenceServer + contances.searchPath + '?' + qs.stringify({ q: sb.toString() })
            }
            , (function (error, response, body) {
                // body is the decompressed response body
                //console.log('server encoded the data as: ' + (response.headers['content-encoding'] || 'identity'))
                //console.log('the decoded data is: ' + body)

                if (error) {
                    res.json({ success: false, error: error }).end();
                    this();
                }
                else
                {
                    var searchResult = body.split('\n');
                    var tempObject = new Object();

                    searchResult.forEach(function (result) {

                        if (result)
                        {
                        var object = JSON.parse(result);

                        var tsentence = object.document.sentence;
                        var tdestopwords = sentenceHelper.removeStopwords(tsentence);
                        var tkgram = sentenceHelper.wordskgram(tdestopwords);
                        //console.log(tkgram);


                        if (!tempObject.hasOwnProperty(object.document.docId))
                        {
                            tempObject[object.document.docId] = sentenceHelper.newJaccard(kgramWords.slice(), tkgram, index + '-' + object.document.order);
                        }
                        else
                        {
                            var newJaccardRes = sentenceHelper.newJaccard(kgramWords.slice(), tkgram, index + '-' + object.document.order);
                            if (tempObject[object.document.docId].dupilicate < newJaccardRes.dupilicate)
                            {
                                tempObject[object.document.docId] = newJaccardRes;
                            }
                        }

                        }

                    });

                    Object.keys(tempObject).forEach(function (key) {
                        if (!checkObject.hasOwnProperty(key)) {
                            checkObject[key] = {};
                            checkObject[key].orders = [];

                            checkObject[key].dupilicate = tempObject[key].dupilicate;
                            checkObject[key].all = tempObject[key].all;
                            checkObject[key].orders.push(tempObject[key].order);
                        }
                        else {
                            checkObject[key].dupilicate += tempObject[key].dupilicate;
                            checkObject[key].all += tempObject[key].all;
                            checkObject[key].orders.push(tempObject[key].order);
                        }
                    })

                    this();
                }
      
            }).bind(this)
            ).on('data', function (data) {
                // decompressed data as it is received
                //console.log('decoded chunk: ' + data)
            })
            .on('response', function (response) {
                // unmodified http.IncomingMessage object
                response.on('data', function (data) {
                    // compressed data as it is received
                    //console.log('received ' + data.length + ' bytes of compressed data')
                })
            });

        }
  
    })
        .seq(function () {
            res.json({ contentLength: contentLength, checkResult: checkObject });
            this();
        });
});



module.exports = router;