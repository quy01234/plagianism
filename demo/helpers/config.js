// configuration =========
// =======================

//var mongoose = require('mongoose');
//var sql = require('mssql');
var morgan = require('morgan');

var bodyParser  = require('body-parser');
var cors = require('cors');

var config = require('../config.js'); // get our config file
//var multer = require('multer');

//mongoose.connect(config.mongoDatabase, function(error) {
//    if (error) {
//        console.log(error);
//        process.exit(1);
//    }
//	else
//		console.log("connect mongo db sucess");
//}); // connect to mongo database


//sql.connect(config.sqlDatabase, function (error) {
//    if (error) {
//        console.log(error);
//        process.exit(1);
//    }
//    else
//        console.log("connect sql db sucess");

//}); // connect to sql database

module.exports.setup = function (app) {


		//use cors to get restful data to angular
		app.use(cors());
		// use body parser so we can get info from POST and/or URL parameters
		app.use(bodyParser.urlencoded({ extended: true }));
		app.use(bodyParser.json());
		//app.use(multer());

		// use morgan to log requests to the console
		app.use(morgan('dev'));

};


// =======================