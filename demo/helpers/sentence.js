// sententce =========
// =======================


var stopwords = require('../stopwords.js'); // get our config file


module.exports.removeStopwords = function (entry) {

    var x;
    var y;
    var word;
    var regex_str;
    var regex;
    var cleansed_string = entry;

    // Split out all the individual words in the phrase
    var words = entry
            .replace(/\s+/g, " ")
            .toLowerCase()
            .split(" ");

    // Review all the words

    for (x = 0; x < words.length; x++) {
        word = words[x];   // Trim the word and remove non-alpha
        // For each word, check all the stop words
        for (y = 0; y < stopwords.length; y++) {
            // Get the current word
            

            // Get the stop word
            stopword = stopwords[y];
            //console.log(stopword,word);

            // If the word matches the stop word, remove it from the keywords
            if (word.toLowerCase() == stopword) {
                // Build the regex
                regex_str = "^\\s*" + stopword + "\\s*$";      // Only word
                regex_str += "|^\\s*" + stopword + "\\s+";     // First word
                regex_str += "|\\s+" + stopword + "\\s*$";     // Last word
                regex_str += "|\\s+" + stopword + "\\s+";      // Word somewhere in the middle
                regex = new RegExp(regex_str, "ig");

                // Remove the word from the keywords
                cleansed_string = cleansed_string.replace(regex, " ");
            }
        }
    }
    return cleansed_string.replace(/^\s+|\s+$/g, "");

};

module.exports.wordskgram = function (sentence,gram) {
    gram = typeof gram !== 'undefined' ? gram : 2;
    var res = [];
    var words = sentence
                .replace(/[.,?!;()"'-]/g, "")
                .replace(/\s+/g, " ")
                .toLowerCase()
                .split(" ");
    var count = words.length - gram + 1
    if (count < 0) {
        count = 0;
    }

    for(var i = 0; i < count;i++)
    {
        res.push(words[i] + " " + words[i + 1]);
    }

    return res;
};

module.exports.countwords = function (sentence,gram) {
    //var res = []

    gram = typeof gram !== 'undefined' ? gram : 2;

    var words = sentence
                .replace(/[.,?!;()"'-]/g, " ")
                .replace(/\s+/g, " ")
                .toLowerCase()
                .split(" ");

    //words.forEach(function (word) {
    //    if (!(index.hasOwnProperty(word))) {
    //        index[word] = 0;
    //    }
    //    index[word]++;
    //});
    var count = words.length - gram + 1
    if (count < 0)
    {
        count = 0;
    }
    return count;
}


module.exports.newJaccard = function (sentenceArray1, sentenceArray2, order) {
    //var res = []

    //console.log(sentenceArray1, sentenceArray2);
    gram = typeof gram !== 'undefined' ? gram : 2;

    console.log(order);

    var unionArray = [];
    var joinArray = [];
    for (var i = 0; i < sentenceArray1.length; i++)
    {
        for (var j = 0; j < sentenceArray2.length; j++)
        {
            if(sentenceArray1[i] === sentenceArray2[j])
            {
                unionArray.push(sentenceArray1[i]);
                joinArray.push(sentenceArray1[i]);

                sentenceArray1.splice(i--, 1);
                sentenceArray2.splice(j--, 1);
                break;
            }
        }
    }

    joinArray = joinArray.concat(sentenceArray1).concat(sentenceArray2);

    //console.log(unionArray, joinArray);

    return { dupilicate: unionArray.length, all: joinArray.length ,order:order};
}


// =======================