﻿Kính gửi thầy Thành,

Em đang làm báo cáo để nhờ thầy góp ý cho hệ thống, tuy nhiên nó chưa đủ chi tiết và khi nộp bài cũng không đủ dung lượng nên em sẽ nộp bổ sung bản chi tiết sau.

Về phần hệ thống ban đầu đã đạt được một số kết quả, tính được sự trùng khớp trong câu.

Các thành phần trong hệ thống thì em đã xây dựng được một API có các chức năng upload, phân tích câu, đẩy vào index của searchengine, lấy được thông tin từ tài liệu, em xin gửi một số kết quả qua screenshot.

Phần sourcecode + báo cáo em vẫn làm thêm và sẽ cập nhật vào địa chỉ https://quy01234@bitbucket.org/quy01234/plagianism.git
em sẽ liên lạc và nhờ thầy góp ý thêm ạ,

Em cảm ơn thầy