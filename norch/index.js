const config = require('./config.js');

var optionsSentences = {indexPath:'sentences',
    port: config.sentencesServer,
    logLevel: 'info',
    addConfigs: config.addConfigs
}
				
				
require('norch')(optionsSentences, function(err, norch) {
  console.log('sentences norch server started on port '+optionsSentences.port);
})


var optionsDocument = {indexPath:'document',
    port: config.documentServer,
				logLevel: 'info',
				disableLogo: true,
				addConfigs: config.addConfigs
				}
require('norch')(optionsDocument, function(err, norch) {
  console.log('document norch server started on port '+optionsDocument.port);
})