﻿module.exports = {
    'sentencesServer':8080,
    'documentServer': 8081,
    'addConfigs': {nGramLength:2, stopwords: require('./stopwords.js')  }
};

